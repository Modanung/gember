#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#define DEMARGIN(x) x->setMargin(0); x->setContentsMargins(0,0,0,0)

class MainWindow: public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
};
#endif // MAINWINDOW_H
