
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

#include "mainwindow.h"

MainWindow::MainWindow(QWidget* parent): QMainWindow(parent)
{
    setWindowIcon(QIcon::fromTheme("gmusicbrowser"));

    auto connectionWidget{  new QWidget{} };
    auto connectionRow{ new QHBoxLayout{} };
    connectionWidget->setLayout(connectionRow);
    connectionRow->addWidget(new QPushButton{ "Connect" } );
    connectionRow->addWidget(new QPushButton{ "..." } );
    connectionRow->setStretch(0, 1);

    auto controls{      new QWidget{} };
    auto controlsRow{   new QHBoxLayout{} };
    controlsRow->addWidget(new QPushButton{ "Prev" });
    controlsRow->addWidget(new QPushButton{ "Play" });
    controlsRow->addWidget(new QPushButton{ "Next" });
    controls->setLayout(controlsRow);

    auto centralWidget{     new QWidget{} };
    auto centralColumn{     new QVBoxLayout{} };
    centralColumn->addWidget(connectionWidget);
    centralColumn->addWidget(controls);
    centralWidget->setLayout(centralColumn);
    setCentralWidget(centralWidget);

    controls->setEnabled(false);

    DEMARGIN(connectionRow);
    DEMARGIN(controlsRow);
    DEMARGIN(centralColumn);
    setFixedSize(0, 0);

    show();
}

MainWindow::~MainWindow()
{
}

